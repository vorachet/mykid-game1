import { Scene } from 'phaser'
import sky from '@/game/assets/sky.png'
import fa from '@/game/assets/fa.png'
import fj from '@/game/assets/fj.png'
import fb from '@/game/assets/fb.png'

import thudMp3 from '@/game/assets/thud.mp3'
import thudOgg from '@/game/assets/thud.ogg'


export default class BootScene extends Scene {
  constructor () {
    super({ key: 'BootScene' })
  }

  preload () { 
    this.load.image('sky', sky)
    this.load.image('fa', fa)
    this.load.image('fj', fj)
    this.load.image('fb', fb)
    this.load.audio('thud', [thudMp3, thudOgg])
  }

  create () {
    this.scene.start('PlayScene')
  }
}
