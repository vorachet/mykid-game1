import { Scene } from 'phaser'

export default class PlayScene extends Scene {
  constructor () {
    super({ key: 'PlayScene' })
  }

  create () {
    this.add.image(400, 300, 'sky')

    const face = this.physics.add.image(400, 200, 'fa')
    face.setCollideWorldBounds(true)
    face.body.onWorldBounds = true 
    face.setBounce(1)
    face.setVelocity(200, 20)

    const face2 = this.physics.add.image(500, 500, 'fj')
    face2.setCollideWorldBounds(true)
    face2.body.onWorldBounds = true 
    face2.setBounce(1)
    face2.setVelocity(200, 20)

    const face3 = this.physics.add.image(300, 300, 'fb')
    face3.setCollideWorldBounds(true)
    face3.body.onWorldBounds = true 
    face3.setBounce(1)
    face3.setVelocity(400, 20)

    this.sound.add('thud')
    this.physics.world.on('worldbounds', () => {
      this.sound.play('thud', { volume: 0.75 })
    })
  }

  update () {
  }
}
